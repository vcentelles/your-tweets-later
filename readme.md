# README: Your tweets, later

## Introduction ##
"__Your tweets, later__" is a project for unfollowing users in twitter temporally. 

The user will be able to specify when and the interval of time that he wants unfollow a  twitter user. It is a simple task: "Your tweets, later" schedules a "unfollow" task and, them, then a specified time, another "follow" task.

This project is a excuse for check and test several technologies, frameworks and libraries (and, of course, for fun). For this reason, it has not a defined structured and maybe a testing base.

## Description ##
At this moment, this project is divided in three parts: client, queue and web.

* __client__: simple script for list friend, unfollow and follow twitter users. It use the "queue" part for the "follow" and "unfollow" actions (_in development_). 
* __queue__: uses celery and rabbitmq for enqueue the follow and unfollow operations (_in development_).
* __web__: (_NEW_) in the future, it will use django for showing the programmed "unfollows" and "follows" operations (_in development_).

This app use "[python_twitter](https://github.com/bear/python-twitter)" for calling the twitter API.

## Installation ##
You can install all the dependencies with the _requeriments.txt_ file:

    pip install -r requirements.txt
    
I recommend use the [virtualenv](http://www.virtualenv.org/en/latest/) and [virtualwrapper](http://virtualenvwrapper.readthedocs.org/en/latest/) tools for work with python and install the dependencies.


__author__ = 'victor'

import logging
import os
import twitter

logger = logging.getLogger(__name__)

## Set up configuration #######################################################
CONSUMER_KEY = os.getenv("YTL_TWITTER_CONSUMER_KEY")
CONSUMER_SECRET = os.getenv("YTL_TWITTER_CONSUMER_SECRET")
ACCESS_TOKEN_KEY = os.getenv("YTL_TWITTER_TOKEN_KEY")
ACCESS_TOKEN_SECRET = os.getenv("YTL_TWITTER_TOKEN_SECRET")
###############################################################################

logger.debug("=" * 80)
logger.debug("Twitter testing")
logger.debug("CONSUMER_KEY: %s" % CONSUMER_KEY)
logger.debug("CONSUMER_SECRET: %s" % CONSUMER_SECRET)
logger.debug("ACCESS_TOKEN_KEY: %s" % ACCESS_TOKEN_KEY)
logger.debug("ACCESS_TOKEN_SECRET: %s" % ACCESS_TOKEN_SECRET)
logger.debug("=" * 80)

cache_api = dict()

def obtain_twitter_api(consumer_key=CONSUMER_KEY,
                       consumer_secret = CONSUMER_SECRET,
                       access_token_key=ACCESS_TOKEN_KEY,
                       access_token_secret=ACCESS_TOKEN_SECRET,
                       renew_api=False):
    """
    Obtain a Api object with the consumer and access data.
    """
    if renew_api or "api" not in cache_api.keys():
        twitter_api = twitter.Api(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN_KEY, ACCESS_TOKEN_SECRET)
        cache_api["api"] = twitter_api
    else:
        twitter_api = cache_api["api"]

    return twitter_api

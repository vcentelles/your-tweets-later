from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.shortcuts import render

admin.autodiscover()

def test(request):
    return render(request, 'base.haml', {"title": "Prueba de template"})

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'login', test)
)

import sys
from queue.tasks import follow, unfollow

from twitter_configuration import obtain_twitter_api

__author__ = 'victor'

api = None

SHOW_FRIENDS = 1
UNFOLLOW_FRIENDS = 2
FOLLOW_USER = 3
EXIT = 0

def show_menu():
    correct_option = False

    while not correct_option:
        print "\n\nOptions:"
        print " 1) Show friends"
        print " 2) Unfollow friend"
        print " 3) Follow user"
        print
        print " 0) Exit"
        print

        try:
            option = int(raw_input("Choose your option: "))
            if option >= 0 and option <= 3:
                correct_option = True
        except ValueError, e:
            print >> sys.stderr, "Please, choose a option in correct interval [0-3]"
            continue
    return option


def show_friends():
    """Show the friends of the twitter user"""
    api = obtain_twitter_api()
    st = api.GetFriends()
    #print "Type: ", type(st) # st is a list
    for friend in st:
        print "> Name: %s  > Screen Name: %s" % (friend.name, friend.screen_name)

    return st


def send_unfollow():
    """Unfollow a specified user"""
    friend_name = raw_input("Please, enter the username to unfollow: ")
    #st = api.DestroyFriendship(screen_name=friend_name)
    res = unfollow.delay(friend_name)
    st = res.get(timeout=2)
    print type(st)
    print st.status
    for i in st.__dict__.iteritems():
        print i

    return st

def send_follow():
    """Follow a specified user"""
    friend_name = raw_input("Please, enter the user name to follow: ")
    #st = api.CreateFriendship(screen_name=friend_name)
    res = follow.delay(friend_name)
    st = res.get(timeout=2)
    print type(st)
    for i in st.__dict__.iteritems():
        print i

    return st

def init_ytl():
    """Init the menu for choosing the options"""
    while True:
        option = show_menu()
        if option == SHOW_FRIENDS:
            show_friends()
        elif option == FOLLOW_USER:
            send_follow()
        elif option == UNFOLLOW_FRIENDS:
            send_unfollow()
        elif option == 0:
            break


if __name__ == "__main__":
    import logging.config

    logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
    init_ytl()
    print "Finish the execution..."
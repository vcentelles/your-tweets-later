__author__ = 'victor'

import sys
sys.path.append("..")

from celery import Celery
from twitter_configuration import obtain_twitter_api

api = obtain_twitter_api()
app = Celery('queue', backend="amqp", broker='amqp://guest@localhost//', include=["queue.tasks"])


@app.task
def unfollow(friend_name):
    print "Unfollow: ", friend_name
    st = api.DestroyFriendship(screen_name=friend_name)
    return st


@app.task
def follow(friend_name):
    """Follow to user"""
    print "Follow: ", friend_name
    st = api.CreateFriendship(screen_name=friend_name)
    return st


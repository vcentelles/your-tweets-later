/*JQuery functions by Biicode*/

// Current Cell data
var currentData = null;
var currentURL = null;
var decorator = null;				// Global variable for code decorations

/* Showing/hiding tool buttons (history, raw, deps, code) */
function showTools(show) {
	document.getElementById("tools-content").style.display = show ? "block" : "none";
}

/* Main json processing function */
function processData(data) {
	if (decorator) decorator.stop();
	var codeDiv = $("#code");
	if (!codeDiv.length) {
		$("#browser_content").append("<div id='code'></div>");
		codeDiv = $("#code");
	} else {
		codeDiv.html("");
	}
	var rawButton = document.getElementById("raw-btn");
	var depsButton = document.getElementById("deps-btn");
	if (data.mimetype==="text/plain") {
		decorator = new BiiCodeDecorator(data, codeDiv);
		rawButton.style.display = "inline";
		depsButton.style.display = "inline";
	} else {
		var html_content = "<img src='data: "+data.mimetype+";base64,"+data.content+"'>";
		codeDiv.append(html_content);
		rawButton.style.display = "none";
		depsButton.style.display = "none";
		document.getElementById("history").style.display = "none";
		$('#code').show();
		$('#deps').hide();
	}
	// Change code browser title
	document.getElementById("cell-name").innerHTML = data.name;
	// show cell dependencies
	loadDependencies(data);
	// show cell history
	loadHistory(data);
	// Finally, show tools as needed
	showTools(true);
	// Open dirs
	// Following code allows to open the tree folders structure
	// when loading a new cell, but not sure if needed when using Ajax cell loading…
	// So right now it is disabled
	// initializeJSTree(function() {
	// 	var path_array = data.path.split("/");
	// 	var dirs_array = [];	// folders that need to be opened
	// 	for (var i=1; i<path_array.length; i++)
	// 		dirs_array.push(path_array.slice(0,i).join("/"));
	// 	return dirs_array;
	// 	}(),'/static/images/file.png');
}

/* Loads file dependencies in browser */
function loadDependencies(data) {
	var dependencies = data.dependencies;
	var depsDiv = document.getElementById("deps");
	depsDiv.innerHTML = "";
	if (!dependencies) return;
	for (var dep in dependencies) {
		var list = dependencies[dep];
		var dep_h6 = document.createElement("h6");
		dep_h6.innerHTML = dep;
		var dep_list = document.createElement("ul");
		for (var d=0; d<list.length; d++) {
			dep_list.innerHTML+="<li>"+list[d]+"</li>";
		}
		depsDiv.appendChild(dep_h6);
		depsDiv.appendChild(dep_list);
	}
}

/* Loads file history in browser */
function loadHistory(data) {
	var listOne = document.getElementById("diff_selector_one");
	var listTwo = document.getElementById("diff_selector_two");
	listOne.innerHTML = "";
	listTwo.innerHTML = "";
	var history = data.history;
	if (!history) return;
	var path = data.path;
	var url = data.url;
	for (var i=0; i<history.length; i++) {
		var op1 = document.createElement("option");
		op1.innerHTML = history[i].msg + " (" + history[i].date + ")";
		var op2 = op1.cloneNode();
		op1.value = i+"/cells/"+path;
		op2.value = url + "/versions/" + i + "/diffs/";
		if (i==data.version) op2.selected = "selected";
		listOne.appendChild(op1);
		listTwo.appendChild(op2);
	}
}

/* Object for code highlighting/decoration */
function BiiCodeDecorator(data, codeDiv) {
	var THAT = this;
	this.lang = data.lang;						// Code language
	this.codeDiv = codeDiv;						// Div to put highlighted code
	this.n_lines = 80;							// number of code lines per chunk
	this.currentLine = 0;						// Current position of code highlighter
	this.intervalTime = 50;
	this.timeout = null;
	this.highlighter = null;
	this.code_container = null;					// highlighted code goes here
	this.number_container = null;				// number lines goe here
	this.current_line = 0;						// current code line
	this.codeLines = data.content.split("\n");	// Array of code lines
	this.codeLinesN = this.codeLines.length;	// Total number of code lines
	this.progress_container = document.getElementById("progress_container");
	this.progress = document.getElementById("progress");	// progress bar div
	/* Stop interval. Needed when loading another file */
	this.stop = function () {
		clearTimeout(THAT.timeout);
		// Reset progressbar
		THAT.progress.style.width = "0";
		THAT.progress_container.style.opacity = 0;
	};
	function addPreNode(text) {
		var pre = document.createElement("pre");
		pre.className = "sunlight-highlight-" + THAT.lang;
		pre.textContent = text;
		THAT.codeDiv.append(pre);
	}
	/* Main highlighting function */
	this.doHighlight = function () {
		var chunk = THAT.codeLines.splice(0,THAT.n_lines).join("\n")+"\n"; // new chunk of code lines
		if (THAT.code_container) {
			var nodes = THAT.highlighter.highlight(chunk, THAT.lang).getNodes();
			for (var i=0; i<nodes.length; i++) {
				THAT.code_container.append(nodes[i]);
			}
			// line numbers
			for (var i_end = THAT.current_line+THAT.n_lines; THAT.current_line<i_end; THAT.current_line++) {
				var line_id = "sunight-1-line-"+THAT.current_line;
				THAT.number_container.append("<a name="+line_id+" href=#"+line_id+">"+THAT.current_line+"</a>\n");
			}
			var percent = (THAT.codeLinesN-THAT.codeLines.length)/THAT.codeLinesN;
			THAT.progress.style.width = (percent)*100+"%"; // progress bar
			if (THAT.codeLines.length===0) THAT.stop();
		} else {
			addPreNode(chunk);
			Sunlight.highlightAll();	// Initial highlighting
			THAT.code_container = $(".sunlight-highlighted");
			THAT.number_container = $(".sunlight-line-number-margin");
		}
		if (THAT.codeLines.length) {
			// There are more codelines
			THAT.current_line = THAT.n_lines;
			THAT.progress_container.style.opacity = 1;
			THAT.timeout = setTimeout(THAT.doHighlight, THAT.intervalTime);
		}
	};
	if (data.lang==="text") {
		// Plain text is not formatted
		addPreNode(data.content);
		return;
	}
	// Now, load the parser for current language
	$.getScript(
		STATIC_PATH+'sunlight-1.19.0/lang/sunlight.'+THAT.lang+'-min.js',
		function() {
			THAT.highlighter = new Sunlight.Highlighter();
			THAT.doHighlight();
		}
	);
}

//Initialize JS TREE
function initializeJSTree(openDirs, static_file_image_path) {
	$("#tree").jstree({
		"core" : {
			"initially_open" : openDirs
		},
		"themes" : {
			"theme" : "classic",
			"dots" : true,
			"icons" : true
		},
		'types' : {
			'types' : {
				'file' : {
					'icon' : {
						'image' : static_file_image_path
					}
				},
			}
		},
		"plugins" : [ "themes", "html_data", "ui", "types" ]
	});

	$("#tree").delegate("a", "click", function(e) {
		if ($("#tree").jstree("is_leaf", this)) {
			// document.location.href = this;
			// dynamically load document for current leaf
			currentURL = this.href;
			$.ajax(this.href.replace("?json", "") + "?json",
			{
				complete: function(req, status) {
					// New file has been fetched
					if (status==="success") {
						currentData = JSON.parse(req.responseText);
						// Change web browser url
						if (window.history.pushState) {
							window.history.pushState({"data": currentData}, "", currentURL);
							window.onpopstate = function(e) {
								if (e.state) {
									currentData = e.state.data;
									processData(currentData);
								}
							}
						}
						processData(currentData);
					}
				}
			});
		} else {
			$("#tree").jstree("toggle_node", this);
		}
	});
}

function ajaxRequest(url, id, params){
	if ($(id).text().trim() == ""){
		$.ajax({
			url: "/ajax/" + url,
			dataType: "json",
			type: "GET",
			data: params,
			context: document.body
		}).done(function (data) {
			if (data.error){
				$(id).html("<p>" + data.error + "</p>")
			}
			else{
				$(id).html(data.response)
			}
		});
	}
}

// Actions binded to buttons, dropdowns...
$(document).ready(


		function() {

		     //spin.js config
            var opts = {
              lines: 13, // The number of lines to draw
              length: 0, // The length of each line
              width: 4, // The line thickness
              radius: 18, // The radius of the inner circle
              corners: 1, // Corner roundness (0..1)
              rotate: 26, // The rotation offset
              direction: 1, // 1: clockwise, -1: counterclockwise
              color: '#000', // #rgb or #rrggbb or array of colors
              speed: 1.3, // Rounds per second
              trail: 54, // Afterglow percentage
              shadow: false, // Whether to render a shadow
              hwaccel: false, // Whether to use hardware acceleration
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: 60, // Top position relative to parent in px
              left: 'auto' // Left position relative to parent in px
            };
            var tree = document.getElementById('tree-div');
            var spinner_tree = new Spinner(opts).spin(tree);

            //when tree is loaded, spin is stoped and div is shown
            $("#tree").bind("loaded.jstree", function () {
                spinner_tree.stop();
                $('#tree').show();
            });
            // Init tree with folders path
            if (typeof cellPath != 'undefined')
                initializeJSTree(cellPath,STATIC_PATH+'images/file.png');

			// Highlight code if present
			Sunlight.highlightAll();
			// Remember if feature tabs on blocks has been opened
			var alreadyOpened = localStorage['opened'];

			$(".accordion-toggle ").click(function() {
				if (!alreadyOpened) {
					// open popup
					localStorage['opened'] = "yes";
				} else if (alreadyOpened == "yes")
				localStorage['opened'] = "no";
				else
					localStorage['opened'] = "yes";
			});

			$(".tab-title ").click(function() {
				$("#collapse-features").collapse("show");
				localStorage['opened'] = "yes";
			});

			if (alreadyOpened == "yes")
				$("#collapse-features").collapse("show");

			/*--------------------Blocks---------------------*/
			// Make the selected tab visible
			$('#tabs a').click(function(e) {
				e.preventDefault();
				$(this).tab('show');
			});

			// Select branchs in block section by clicking on dropdown
			$('#branch_selector').change(function() {
	    	    document.location.href = $(this).val();
            });

			// Description edition
			$('#edit-desc-button').on('click', function(e) {
				e.preventDefault();

				$('#edit-desc').show();
				$('#desc').hide();
			});
			$('#cancel-desc-button').on('click', function(e) {
				e.preventDefault();

				$('#desc').show();
				$('#edit-desc').hide();
			});

			/*-------------------Browser---------------------*/
			// Set the same height for sidebar and browser
			$("#sidebar").height($("#browser").height());

			// For diffs, is used also on diff for cells template
			$('#diff-button').on(
				'click',
				function(e) {
					var url = $('#diff_selector_two').val() + ""
					+ $('#diff_selector_one').val();
					document.location.href = url;
				});

			// Show/hide history section
			$('#history-btn').on('click', function(e) {
				$('#history').toggle();
				$('#history-btn').toggleClass("active");
			});
			// Raw code button
			$('#raw-btn').on('click', function() {
				// raw document opens in a new tab
				window.open(window.location+"?raw", '_blank');
			});

			// Show/hide deps section
			$('#deps-btn').on('click', function(e) {
				$('#deps').show();
				$('#code').hide();

				$('#deps-btn').addClass("active");
				$('#code-btn').removeClass("active");
			});

			// Show/hide code section
			$('#code-btn').on('click', function(e) {
				$('#code').show();
				$('#deps').hide();

				$('#deps-btn').removeClass("active");
				$('#code-btn').addClass("active");
			});

			// Resizable sidebar on browser
			$("#sidebar").resizable({
				handles : "e",
				maxWidth : 700,
				minWidth : 140,
				grid : 70
			});

			var sidebar_width = $("#sidebar").width();

			$("#sidebar").on(
				"resize",
				function() {

					current_code_div_witdh = $("#browser_content").width();
					new_sidebar_width = $("#sidebar").width();

					$("#browser_content").width(
						current_code_div_witdh
						+ (sidebar_width - new_sidebar_width));
					sidebar_width = new_sidebar_width
				});

		});
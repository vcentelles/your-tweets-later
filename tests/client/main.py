# -*- coding: utf-8 -*-

import mock
import os
import unittest
import twitter

from client.main import show_friends, send_follow
from twitter_configuration import obtain_twitter_api

__author__ = 'victor'


class MainTests(unittest.TestCase):

    def setUp(self):
        CONSUMER_KEY = os.getenv("TEST_YTL_TWITTER_CONSUMER_KEY")
        CONSUMER_SECRET = os.getenv("TEST_YTL_TWITTER_CONSUMER_SECRET")
        ACCESS_TOKEN_KEY = os.getenv("TEST_YTL_TWITTER_TOKEN_KEY")
        ACCESS_TOKEN_SECRET = os.getenv("TEST_YTL_TWITTER_TOKEN_SECRET")

        #Initialize the api (change the keys for other test users)
        obtain_twitter_api(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN_KEY, ACCESS_TOKEN_SECRET)

    def test_show_friends(self):
        st = show_friends()
        self.assertIsInstance(st, list)
        if st:
            self.assertIsInstance(st[0], twitter.User)

    def test_send_follow(self):
        with mock.patch("__builtin__.raw_input", return_value="@victorcentelles"):
            st = send_follow()
            self.assertIsInstance(st, twitter.User)
            self.assertEqual(st.name, u"Víctor Centelles")
            self.assertEqual(st.screen_name, "victorcentelles")




